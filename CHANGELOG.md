# Changelog

## [1.3.0] - 2024-10-25

### Added

- Backup retention mechanism


## [1.2.5] - 2024-10-21

### Added

- Restoration script templates

### Changed

- `lib/` -> `src/`

### Removed

- BS parameter


## [1.2.4] - 2024-04-26

### Fixed

- `LOCAL_RSYNC_PATH`: replace `rsync` alias (won't work in a non-interactive shell).


## [1.2.3] - 2024-04-16

### Added

- `tar.sh`, `dd.sh`: Remove old local backups


## [1.2.2] - 2024-04-05

### Added

- Exclude patterns for `mirror_remote.sh` and `mirror_local.sh`


## [1.2.1] - 2024-03-25

### Added

- Enabled relative path in `tar` files.
- Date in log timestamps.
- `rsync` alias hint.

### Fixed

- `rsync` destination path not quoted for mirroring scripts.


## [1.2.0] - 2023-02-04

### Changed

- Simplified crontab task registry
    - `/etc/cron.d/tar-nas-console` which is copied from `conf/cron.d.template`

### Fixed

- Worked around `rsync` server-client version mismatch

### Removed

- BATS unit tests for the old crontab registry


## [1.1.0] - 2022-02-07

### Added

- Backing up with dd
    a. Pipe to a NAS directly via SSH (`dd_ssh_pipe.sh`)
    b. Compress to a local destination and then upload to a NAS (`dd.sh`)


## [1.0.0] - 2022-01-23

### Added (summary of past changes)

1. Backing up with `tar`
    a. Pipe to a NAS directly via SSH (`tar_ssh_pipe.sh`)
    b. Compress to a local destination and then upload to a NAS (`tar.sh`)
2. Backing up with `rsync`
    a. Mirror to a local destination
    b. Mirror to a NAS

### Changed

* `rsync` now writes error messages to the log
