#!/usr/bin/env bash
#"""
# dd to NAS directly, with compression
#"""

# Need not source global config because this file is sourced by tasks.

echo "$(date '+%F %T') [Debug] dd_ssh_pipe.sh begins..." >> "$LOG_FILE"

#"""
# check
#"""

# folder count
N_SRC=${#SOURCE_ARR[@]}
N_DEST=${#DEST_ARR[@]}

if (( N_SRC != N_DEST )); then
  echo "$(date '+%F %T') [Error] source/destination/bs counts didn't match!" >> "$LOG_FILE"
  exit 1
fi

# source device existence
for it in "${SOURCE_ARR[@]}"; do
  if [ ! -b "$it" ]; then
    echo "$(date '+%F %T') [Error] Bad source device: '$it'" >> "$LOG_FILE"
  fi
done


#"""
# Build command
#"""

# Extension
if [ "$COMP_METHOD" == "gzip" ]; then
  COMP_EXT="img.gz"
  COMP_CMD="gzip"
elif [ "$COMP_METHOD" == "zstd" ]; then
  COMP_EXT="img.zst"
  COMP_CMD="zstd"
elif [ "$COMP_METHOD" == "zip" ]; then
  COMP_EXT="img.zip"
  COMP_CMD="zip"
else
  echo "$(date '+%F %T') [Debug] Set unknown COMP_METHOD='$COMP_METHOD' to 'gzip'" >> "$LOG_FILE"
  COMP_METHOD="gzip"
  COMP_EXT="img.gz"
  COMP_CMD="gzip"
fi

TIMESTAMP="$(date +%FT%H-%M-%S)"

#"""
# Execute
#"""

echo "$(date '+%F %T') [Debug] begin mirroring..." >> "$LOG_FILE"

for (( i=0; i < N_SRC; ++i )); do

  T1i=$SECONDS

  CAT_CMD="cat > '$DEST_NAS/${DEST_ARR[i]}_$TIMESTAMP.$COMP_EXT'"

  dd if=${SOURCE_ARR[i]} bs=1M | \
      $COMP_CMD | \
      ssh -p $NAS_SSH_PORT $NAS_USER@$NAS_HOST "$CAT_CMD" \
      >> "$LOG_FILE" 2>&1

  EXIT_CODE=$?
  T2i=$SECONDS

  # check exit status
  if [ $EXIT_CODE -eq 0 ]; then
    echo "$(date '+%F %T') [Debug] '${SOURCE_ARR[i]}' OK in $((T2i - T1i)) seconds!" >> "$LOG_FILE"
  else
    echo "$(date '+%F %T') [Error] Bad exit code '$EXIT_CODE' upon '${SOURCE_ARR[i]}'! $((T2i - T1i)) seconds elapsed." >> "$LOG_FILE"
    exit $EXIT_CODE
  fi

done

echo "$(date '+%F %T') [Debug] dd_ssh_pipe.sh done!" >> "$LOG_FILE"
