#!/bin/bash
#"""
# Implement backup retention policy.
#"""

# Need not source global config because this file is sourced by tasks.

echo "$(date '+%F %T') [Debug] retention.sh begins..." >> "$LOG_FILE"

# Match timestamp pattern: group #1=task, #2=yyyy, #3=mm
PATT="(.*)_([0-9]{4})-([0-9]{2})-[0-9]{2}T[0-9]{2}-[0-9]{2}-[0-9]{2}.*"

# Get filenames (with process substitution to handle whitespaces)
readarray -t raw_fnames < <(ssh -p $NAS_SSH_PORT $NAS_USER@$NAS_HOST "ls -A '$NAS_BACKUP_DIR'")

# Collect relevant filenames and info
fnames=()
# task names excluding timestamp and extensions
tasks=()
# for ckecking retention conditions
months=()
quarters=()
years=()

for fname in "${raw_fnames[@]}"; do

    if [[ ! $fname =~ $PATT ]]; then
        continue
    fi

    fnames+=("${BASH_REMATCH[0]}")
    tasks+=("${BASH_REMATCH[1]}")

    yyyy=${BASH_REMATCH[2]}

    mm=${BASH_REMATCH[3]}
    months+=("$yyyy$mm")

    mm_clean=$(echo "$mm" | sed 's/^0*//')
    q=$(( (mm_clean - 1) / 3 + 1 ))
    quarters+=("$yyyy$q")

    years+=("$yyyy")
done

unset raw_fnames
n_files=${#fnames[@]}
echo "$(date '+%F %T') [Debug] $n_files relevant files found in NAS: $NAS_BACKUP_DIR" >> "$LOG_FILE"

# Perform backward scan to find files to be removed
del_fnames=()

for (( i=n_files-1; i>=0; i-- )); do

    # Set counters
    if (( i == n_files - 1 )) || [ "${tasks[i+1]}" != "${tasks[i]}" ]; then
        # The 1st case of each task: reset all counters and flags

        # for global condition
        cnt_latest=$N_LATEST

        # for monthly condition (+1: ignore the current month)
        cnt_month=$((1 + N_MONTHLY_LATEST))
        is_monthly_latest=0

        # for quarterly condition (+1: ignore the current month)
        cnt_quarter=$((1 + N_QUARTERLY_LATEST))
        is_quarterly_latest=0

        # for yearly condition
        is_yearly_latest=1
    else
        # Subsequent cases of each task: propagate the counters and flags

        # 1. global latest counter (always decrement)
        (( cnt_latest-- ))

        # 2. month counter and flag of change
        if [ "${months[i+1]}" != "${months[i]}" ]; then
            (( cnt_month-- ))
            is_monthly_latest=1
        else
            is_monthly_latest=0
        fi

        # 3. quarter counter and flag of change
        if [ "${quarters[i+1]}" != "${quarters[i]}" ]; then
            (( cnt_quarter-- ))
            is_quarterly_latest=1
        else
            is_quarterly_latest=0
        fi

        # 4. flag of year change
        if [ "${years[i+1]}" != "${years[i]}" ]; then
            is_yearly_latest=1
        else
            is_yearly_latest=0
        fi
    fi

    # Mark for deletion when none of the conditions is match
    cond_g=$(( cnt_latest > 0 ))
    cond_m=$(( is_monthly_latest == 1 && cnt_month > 0 ))
    cond_q=$(( is_quarterly_latest == 1 && cnt_quarter > 0 ))
    cond_y=$(( is_yearly_latest == 1 ))
    if ! (( cond_g || cond_m || cond_q || cond_y )); then
        del_fnames+=("${fnames[i]}")
    fi
done

n_deletes=${#del_fnames[@]}
echo "$(date '+%F %T') [Info] $n_deletes files will be deleted..." >> "$LOG_FILE"

# Perform file removal
for fname in "${del_fnames[@]}"; do
    ret=$(ssh -p $NAS_SSH_PORT $NAS_USER@$NAS_HOST "rm '$NAS_BACKUP_DIR/$fname'")
    if [ -z "$ret" ]; then
        echo "$(date '+%F %T') [Info] Deleted: $fname" >> "$LOG_FILE"
    else
        echo "$(date '+%F %T') [Error] Deletion failed: $fname (returned message: $ret)" >> "$LOG_FILE"
    fi
done

echo "$(date '+%F %T') [Debug] retention.sh done!" >> "$LOG_FILE"