#!/usr/bin/env bash
#"""
# Tar first using a local destination and then upload to NAS.
#"""

# Need not source global config because this file is sourced by tasks.

echo "$(date '+%F %T') [Debug] tar.sh begins..." >> "$LOG_FILE"


#"""
# File & Path
#"""

# Extension
if [ "$TAR_METHOD" == "z" ]; then
  TAR_EXT="tar.gz"
elif [ "$TAR_METHOD" == "J" ]; then
  TAR_EXT="tar.xz"
else
  echo "$(date '+%F %T') [Debug] Set unknown TAR_METHOD='$TAR_METHOD' to '${$TAR_METHOD_DEFAULT}'" >> "$LOG_FILE"
  TAR_METHOD="$TAR_METHOD_DEFAULT"
  TAR_EXT="tar.gz"
fi

TIMESTAMP="$(date +%FT%H-%M-%S)"

# Basename
DEST_FNAME="${DEST_MAINNAME}_$TIMESTAMP.$TAR_EXT"

# Full path to NAS
DEST_NAS_FILE="$DEST_NAS/$DEST_FNAME"
echo "$(date '+%F %T') [Debug] Target NAS file: $DEST_NAS_FILE" >> "$LOG_FILE"

# Full path to local drive
# The local drive is connected, but $DEST_LOCALDRIVE may not exist!
mkdir -p "$DEST_LOCALDRIVE"
TAR_FILE="$DEST_LOCALDRIVE/$DEST_FNAME"

echo "$(date '+%F %T') [Debug] Local tar file: $TAR_FILE" >> "$LOG_FILE"


#"""
# Build Tar Command
#"""

TAR_COMMAND="sudo tar -c${TAR_METHOD}p"

# verbose
if [ "$VERBOSE" == "Y" ]; then
  TAR_COMMAND+="v"
fi

# target file
TAR_COMMAND+="f '$TAR_FILE' "

# one-file-system
if [ "$ONE_FS" == "Y" ]; then
  TAR_COMMAND+="--one-file-system "
fi

# apply relative path
if ! [ -z "$TAR_REL_PATH" ]; then
  TAR_COMMAND+="-C '$TAR_REL_PATH' "
fi

# excludes
for it in "${EXCLUDE_ARR[@]}"; do
  TAR_COMMAND+="--exclude='$it' "
done

# sources
for it in "${SOURCE_ARR[@]}"; do
  TAR_COMMAND+="'$it' "
done


#"""
# Execute tar
#"""

echo "$(date '+%F %T') [Debug] Execute tar command: $TAR_COMMAND" >> "$LOG_FILE"

# run
T1=$SECONDS
eval "$TAR_COMMAND"
EXIT_CODE=$?
T2=$SECONDS

# check exit code (0, 1 is OK)
if [ "$EXIT_CODE" -eq 0 ]; then
  echo "$(date '+%F %T') [Info] tar OK in $(( T2 - T1 )) seconds!" >> "$LOG_FILE"
elif [ "$EXIT_CODE" -eq 1 ]; then
  echo "$(date '+%F %T') [Info] tar OK in $(( T2 - T1 )) seconds, but some files were changed during compression (tar exit code = 1)." >> "$LOG_FILE"
else
  echo "$(date '+%F %T') [Error] tar failed (exit code: $EXIT_CODE) in $(( T2 - T1 )) seconds!" >> "$LOG_FILE"
  exit $EXIT_CODE
fi

# remove root privilege
sudo chown $LOCAL_USER:$LOCAL_GROUP "$TAR_FILE"

#"""
# Upload to NAS
#"""

echo "$(date '+%F %T') [Debug] rsync-ing local tar file to NAS: '$TAR_FILE'" >> "$LOG_FILE"

T1=$SECONDS
"$LOCAL_RSYNC_PATH" -rtDe "ssh -p $NAS_SSH_PORT" \
      --rsync-path="mkdir -p '$DEST_NAS' && rsync" \
      "$TAR_FILE" \
      $NAS_USER@$NAS_HOST:$DEST_NAS
EXIT_CODE=$?
T2=$SECONDS

if [ "$EXIT_CODE" -eq 0 ]; then
  echo "$(date '+%F %T') [Info] rsync OK in $(( T2 - T1 )) seconds!" >> "$LOG_FILE"
  # Only when rsync is successful, remove all other files beginning with DEST_MAINNAME
  echo "$(date '+%F %T') [Info] Removing local old backups..." >> "$LOG_FILE"
  cd "$DEST_LOCALDRIVE"
  for f in *; do
    if [[ $f == $DEST_MAINNAME* ]] && [ "$f" != "$DEST_FNAME" ]; then
      rm "$f"
      echo "$(date '+%F %T') [Info] Removed: $f" >> "$LOG_FILE"
    fi
  done
else
  echo "$(date '+%F %T') [Error] rsync failed (exit code: $EXIT_CODE) in $(( T2 - T1 )) seconds!" >> "$LOG_FILE"
  exit $EXIT_CODE
fi

echo "$(date '+%F %T') [Info] tar.sh done!" >> "$LOG_FILE"
