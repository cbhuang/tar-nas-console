#!/usr/bin/env bash
#"""
# Compress to remote path directly through ssh pipe
#"""

# Need not source global config because this file is sourced by tasks.

echo "$(date '+%F %T') [Debug] tar_ssh_pipe.sh begins..." >> "$LOG_FILE"

#"""
# File & Path
#"""

# Extension
if [ "$TAR_METHOD" == "z" ]; then
  TAR_EXT="tar.gz"
elif [ "$TAR_METHOD" == "J" ]; then
  TAR_EXT="tar.xz"
else
  echo "$(date '+%F %T') [Debug] Set unknown TAR_METHOD='$TAR_METHOD' to '${$TAR_METHOD_DEFAULT}'" >> "$LOG_FILE"
  TAR_METHOD="$TAR_METHOD_DEFAULT"
  TAR_EXT="tar.gz"
fi

TIMESTAMP="$(date +%FT%H-%M-%S)"

# Basename
DEST_FNAME="${DEST_MAINNAME}_$TIMESTAMP.$TAR_EXT"

# Full path to NAS
DEST_NAS_FILE="$DEST_NAS/$DEST_FNAME"
echo "$(date '+%F %T') [Debug] Target NAS file: $DEST_NAS_FILE" >> "$LOG_FILE"

#"""
# Build Tar Command
#"""

TAR_COMMAND="sudo tar -c${TAR_METHOD}p"

# verbose
if [ "$VERBOSE" == "Y" ]; then
  TAR_COMMAND+="v"
fi

# target to pipe
TAR_COMMAND+="f - "

# one-file-system
if [ "$ONE_FS" == "Y" ]; then
  TAR_COMMAND+="--one-file-system "
fi

# apply relative path
if ! [ -z "$TAR_REL_PATH" ]; then
  TAR_COMMAND+="-C '$TAR_REL_PATH' "
fi

# excludes
for it in "${EXCLUDE_ARR[@]}"; do
  TAR_COMMAND+="--exclude='$it' "
done

# sources
for it in "${SOURCE_ARR[@]}"; do
  TAR_COMMAND+="'$it' "
done

# ssh command
TAR_COMMAND+="| ssh -p $NAS_SSH_PORT $NAS_USER@$NAS_HOST \"cat > '$DEST_NAS_FILE' \" "


#"""
# Execute tar
#"""

echo "$(date '+%F %T') [Debug] Execute tar command: $TAR_COMMAND" >> "$LOG_FILE"

# create remote directory
ssh -p $NAS_SSH_PORT $NAS_USER@$NAS_HOST "mkdir -p '$DEST_NAS'"

# run
T1=$SECONDS
eval "$TAR_COMMAND"
EXIT_CODE=$?
T2=$SECONDS

# check exit code (0, 1 is OK)
if [ $EXIT_CODE -eq 0 ]; then
  echo "$(date '+%F %T') [Info] tar (ssh-piped) OK in $(( T2 - T1 )) seconds!" >> "$LOG_FILE"
elif [ $EXIT_CODE -eq 1 ]; then
  echo "$(date '+%F %T') [Info] tar (ssh-piped) OK in $(( T2 - T1 )) seconds, but some files were changed during compression (tar exit code = 1)." >> "$LOG_FILE"
else
  echo "$(date '+%F %T') [Error] tar (ssh-piped) failed (exit code: $EXIT_CODE) in $(( T2 - T1 )) seconds!" >> "$LOG_FILE"
  exit $EXIT_CODE
fi

echo "$(date '+%F %T') [Info] tar_ssh_pipe.sh done!" >> "$LOG_FILE"
