#!/usr/bin/env bash
#"""
# dd to a local destination and then upload to NAS, with compression
#"""

# Need not source global config because this file is sourced by tasks.

echo "$(date '+%F %T') [Debug] dd.sh begins..." >> "$LOG_FILE"

#"""
# check
#"""

# folder count
N_SRC=${#SOURCE_ARR[@]}
N_DEST=${#DEST_ARR[@]}

if (( N_SRC != N_DEST )); then
  echo "$(date '+%F %T') [Error] source/destination/bs counts didn't match!" >> "$LOG_FILE"
  exit 1
fi

# source device existence
for it in "${SOURCE_ARR[@]}"; do
  if [ ! -b "$it" ]; then
    echo "$(date '+%F %T') [Error] Bad source device: '$it'" >> "$LOG_FILE"
  fi
done

# ensure destination existence
mkdir -p "$DEST_LOCALDRIVE"

#"""
# Build command
#"""

# Extension
if [ "$COMP_METHOD" == "gzip" ]; then
  COMP_EXT="img.gz"
  COMP_CMD="gzip"
elif [ "$COMP_METHOD" == "zstd" ]; then
  COMP_EXT="img.zst"
  COMP_CMD="zstd"
elif [ "$COMP_METHOD" == "zip" ]; then
  COMP_EXT="img.zip"
  COMP_CMD="zip"
else
  echo "$(date '+%F %T') [Debug] Set unknown COMP_METHOD='$COMP_METHOD' to 'gzip'" >> "$LOG_FILE"
  COMP_METHOD="gzip"
  COMP_EXT="img.gz"
  COMP_CMD="gzip"
fi

TIMESTAMP="$(date +%FT%H-%M-%S)"

#"""
# dd to a local destination
#"""

echo "$(date '+%F %T') [Debug] begin dd and compression..." >> "$LOG_FILE"

for (( i=0; i < N_SRC; ++i )); do

  T1i=$SECONDS
  dd if=${SOURCE_ARR[i]} bs=1M | \
      $COMP_CMD > "$DEST_LOCALDRIVE/${DEST_ARR[i]}_$TIMESTAMP.$COMP_EXT"
  EXIT_CODE=$?
  T2i=$SECONDS

  # check exit status
  if [ $EXIT_CODE -eq 0 ]; then
    echo "$(date '+%F %T') [Debug] '${SOURCE_ARR[i]}' backed up in $((T2i - T1i)) seconds!" >> "$LOG_FILE"
  else
    echo "$(date '+%F %T') [Error] Bad exit code '$EXIT_CODE' upon backing up '${SOURCE_ARR[i]}'! $((T2i - T1i)) seconds elapsed." >> "$LOG_FILE"
    exit $EXIT_CODE
  fi

done

#"""
# Upload to NAS
#"""

echo "$(date '+%F %T') [Debug] Uploading to NAS..." >> "$LOG_FILE"

for (( i=0; i < N_SRC; ++i )); do

  T1i=$SECONDS
  DEST_MAINNAME="${DEST_ARR[i]}"
  DEST_FNAME="${DEST_MAINNAME}_$TIMESTAMP.$COMP_EXT"
  "$LOCAL_RSYNC_PATH" -rtDe "ssh -p $NAS_SSH_PORT" \
        --rsync-path="mkdir -p '$DEST_NAS' && rsync" \
        "$DEST_LOCALDRIVE/$DEST_FNAME" \
        $NAS_USER@$NAS_HOST:$DEST_NAS/$DEST_FNAME \
        >> "$LOG_FILE" 2>&1

  EXIT_CODE=$?
  T2i=$SECONDS

  # check exit status
  if [ $EXIT_CODE -eq 0 ]; then
    echo "$(date '+%F %T') [Debug] '${SOURCE_ARR[i]}' uploaded in $((T2i - T1i)) seconds!" >> "$LOG_FILE"
    # Only when rsync is successful, remove all other files beginning with DEST_MAINNAME
    echo "$(date '+%F %T') [Info] Removing local old backups..." >> "$LOG_FILE"
    cd "$DEST_LOCALDRIVE"
    for f in *; do
      if [[ $f == $DEST_MAINNAME* ]] && [ "$f" != "$DEST_FNAME" ]; then
        rm "$f"
        echo "$(date '+%F %T') [Info] Removed: $f" >> "$LOG_FILE"
      fi
    done
  else
    echo "$(date '+%F %T') [Error] Bad exit code '$EXIT_CODE' upon uploading '${SOURCE_ARR[i]}'! $((T2i - T1i)) seconds elapsed." >> "$LOG_FILE"
    exit $EXIT_CODE
  fi

done

echo "$(date '+%F %T') [Debug] dd.sh done!" >> "$LOG_FILE"
