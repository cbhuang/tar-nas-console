#!/usr/bin/env bash
#"""
# Mirror backup to a local drive
#"""

echo "$(date '+%F %T') [Debug] mirror_local.sh begins..." >> "$LOG_FILE"

#"""
# Check drive & source paths
#"""

# folder count
N_SRC=${#SOURCE_ARR[@]}
N_DEST=${#DEST_ARR[@]}
if (( N_SRC != N_DEST )); then
  echo "$(date '+%F %T') [Error] Folder count in source/destination didn't match!" >> "$LOG_FILE"
  exit 1
fi

# drive is mounted
if [ ! -d "$LOCALDRIVE_MOUNTPOINT" ]; then
  echo "$(date '+%F %T') [Error] Destination drive not found!" >> "$LOG_FILE"
  exit 1
fi

# source path existence
for it in "${SOURCE_ARR[@]}"; do
  if [ ! -d "$it" ]; then
    echo "$(date '+%F %T') [Error] Bad source folder: '$it'" >> "$LOG_FILE"
  fi
done


echo "$(date '+%F %T') [Debug] begin copying..." >> "$LOG_FILE"
T1=$SECONDS
for (( i=0; i < N_SRC; ++i )); do
  echo "$(date '+%F %T') [Info] begin processing '${SOURCE_ARR[i]}'..." >> "$LOG_FILE"

  #"""
  # Build rsync command
  #"""
  # Note: if errors occurrs, consider replacing "-a" with ""-rlOD" as in mirror_remote.sh

  RSYNC_COMMAND="sudo '$LOCAL_RSYNC_PATH' -a --delete "

  if [ $IGNORE_ERRORS -eq 0 ]; then
    RSYNC_COMMAND+="--ignore-errors "
  fi

  # excluded pattern(s)
  for it in "${EXCLUDE_ARR[@]}"; do
    RSYNC_COMMAND+="--exclude='$it' "
  done

  RSYNC_COMMAND+="'${SOURCE_ARR[i]}/' '${DEST_ARR[i]}' >> '$LOG_FILE' 2>&1"

  #"""
  # Ensure destination path exists (only if the above succeeded)
  #"""
  if [ ! -d "${DEST_ARR[i]}" ]; then
    sudo mkdir -p "${DEST_ARR[i]}"
    sudo chown $LOCAL_USER:$LOCAL_GROUP "${DEST_ARR[i]}"
  fi

  #"""
  # Execute
  #"""
  T1i=$SECONDS
  eval "$RSYNC_COMMAND"
  EXIT_CODE=$?
  T2i=$SECONDS

  # check exit status
  if [ $EXIT_CODE -eq 0 ]; then
    echo "$(date '+%F %T') [Info] '${SOURCE_ARR[i]}' OK in $((T2i - T1i)) seconds!" >> "$LOG_FILE"
  else
    echo "$(date '+%F %T') [Error] Bad rsync exit code '$EXIT_CODE' upon '${SOURCE_ARR[i]}'! $((T2i - T1i)) seconds used." >> "$LOG_FILE"
    exit $EXIT_CODE
  fi
done

T2=$SECONDS
echo "$(date '+%F %T') [Info] mirror_local.sh done in $((T2 - T1)) seconds!" >> "$LOG_FILE"
