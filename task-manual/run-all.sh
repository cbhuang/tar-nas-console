#!/usr/bin/env bash
#"""
# Run all manual tasks in this folder
#"""

# source global config
THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${THIS_DIR}/../conf/global.conf"

# execute jobs
touch "$LOG_FILE"
echo -e "\n===================================================" >> "$LOG_FILE"
echo "    $(date '+%F %T') Running all manual tasks " >> "$LOG_FILE"
echo "===================================================" >> "$LOG_FILE"

# execute files in order
find . -name "*.manual.sh" | sort | while read f
do
  sudo bash "$f"
done

echo -e "==== $(date '+%F %T') manual backup done! ====\n" >> "$LOG_FILE"
