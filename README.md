# Tar-NAS-Console Project Documentation

- Author: Chuan-Bin "Bill" Huang
- Date: 2024-10-25
- Version: 1.3.0
- Contact: cbbh0101@gmail.com

**Disclaimer**: *Hobby & educational project. Try at own risk!*

## About

The project contains my personal Linux backup scripts that has been in
production use since 2017. As of Oct. 2024, 3 laptop migrations has been
successfully performed with this code base as well as several system
restoration events.

The project also serves educational purpose. My clients/students could
expect to learn the following skills:

- Crafting and automating Linux backup tasks
- Crucial parameters of `tar` and `rsync` command
- Writing interactive scripts

## Feature

The project works with a NAS device and locally connected drives.
The following backup modes are available:

1. Backup with `tar`:
  a. Pipe to a NAS directly via SSH
  b. Compress to a local destination and then upload to a NAS

2. Backup with `rsync`:
  a. Mirror to a local destination
  b. Mirror to a NAS

3. Backup with `dd`:
  a. Pipe to a NAS directly via SSH
  b. Compress to a local destination and then upload to a NAS

These are also handy:

- Restoration script template
- Cron scheduling

## Screenshots

### Celebrating the 4th year of production run

<img src="img/Screenshot_4th_year.png" alt="screenshot" width="420"/>

### Example 1: Compress Root to NAS and USB Drive

<img src="img/Screenshot_root.png" alt="screenshot" width="420"/>

### Example 2: Compress Folder to NAS and USB Drive

<img src="img/Screenshot_data.png" alt="screenshot" width="420"/>

### Example 3: Compress Folder(s) to USB Drive

<img src="img/Screenshot_tar-nas.png" alt="screenshot" width="420"/>

### Example 4: Mirror Folder(s) to USB Drive

<img src="img/Screenshot_Mirror.png" alt="screenshot" width="420"/>

## Getting Started

### Prerequisites

1. `bash`
    - `bash`-specific syntax is used, so `zsh` or other shells won't work
2. If backing up to an NAS, SSH connection must be enabled in advance.
3. If backing up filesystem root, `sudo` privilege is required.

**Note**: If neither root privilege nor data compression were needed for your
backups, a customized `rsync` script or [FreeFileSync](https://www.freefilesync.org)
would likely suffice for such a use case.

### Installation

1. Download the project.

   ```bash
   cd /path/to/put/this/project
   git clone https://gitlab.com/cbhuang/tar-nas-console.git
   ```

2. Run `setup.sh` (with `sudo` if root privilege is needed for your tasks).

   ```bash
   cd tar-nas-console
   sudo ./setup.sh
   ```

### Setting Up Tasks

#### 1. Configure

   Under `conf/`, copy `global.conf.template` to `global.conf` and edit.

#### 2. Create Scheduled or Manual Tasks

Tasks templates are stored under `task/`. There are currently 5 types of tasks available:

| No. | Source      | Destination    | Format  | Requires sudo | Template File                    |
| --- | ----------- | -------------- | ------- | ------------- | -------------------------------- |
|  1  | System root | Remote + local | .tar.gz | Yes           | `tar_root.sh.template`           |
|  2  | Data        | Remote + local | .tar.gz | Yes           | `tar_data.sh.template`           |
|  3  | Data        | Remote         | copied  | No            | `data_mirror_remote.sh.template` |
|  4  | Data        | Local          | copied  | No            | `data_mirror_local.sh.template`  |
|  5  | Device      | Remote + Local | .img.gz | Usually Yes   | `dd.sh.template`                 |

Follow these steps to create a task:

1. Copy a task template file to
  a. `task/your-desired-name.sh` for creating a scheduled task.
  b. `task-manual/your-desired-name.manual.sh` for creating a manually-run task.

2. Edit all parameters
    - What to include
    - What to exclude
    - Target NAS directory (remote destination only)
        - Whitespaces may be problematic (Synology `rsync-3.1.2` issue)!
    - Target local directory (local destination only)
    - Compression
    - Local temporary directory for compression (local destination only)

3. (optional) Sort the files. For example, assign prefixes such as `10_` or `99_`.
   Only files ending with `.sh` will be run and in ascending alphanumeric order.

### Setting Up Backup Retention Rules

Application of the rules is completely optional and could be added at any
time. Steps:

1. Create `task/99_backup-retention.sh` from `task/99_backup-retention.sh.template`.
2. Edit the parameters.

## Usage

### Execute

#### Scheduled Tasks

After setting up tasks, edit the `/etc/cron.d/tar-nas-console` created
during installation (for system backups), or create one in user crontab.

<img src="img/Screenshot_crontab.png" alt="crontab screenshot" width="420"/>

#### Scheduled Tasks (Run Manually)

Scheduled task could also be run manually with the following method:

```bash
cd task
sudo bash your-desired-file.sh
```

#### Manual Tasks

```bash
cd task-manual
# run a selected task
sudo bash your-desired-file.manual.sh
# or run all tasks at once
sudo bash run-all.sh
```

### View Logs

The path is set with `$LOG_FILE` in `conf/global.conf`.

<img src="img/Screenshot_Logfile.png" alt="screenshot" width="420"/>

## Uninstallation

Removing all crontab entries created would stop the tasks.

With sudo privilege, one could run

```bash
sudo uninst.sh
```

or simply remove `/etc/cron.d/tar-nas-console` and user crontab entries
(if any) manually.

## Development Info

### Git Branches

- `master`: production branch
- `testing`: development branch

### Project Structure

- `conf/`: global configurations (NAS, port, path, etc.)
- `img/`: screenshots
- `src/`: base code
- `task/`: scheduled tasks
  - `*.sh.template`: task templates
- `task-manual/`: manually-run tasks
  - `run-all.sh`: convenience script to run all manually-run tasks at once
- `test/`: test scripts (requires proper `global.conf` and NAS setup)
- `TEMP`: developer's working folder

### Roadmap

#### Features

- Better handling of log file (to be determined)
  - Attach to system log (`var/log`?)
  - Separate log files for each run
- Detect available space
- Add GNU timeout option
- [How to rsync over ssh when directory names have spaces](https://unix.stackexchange.com/questions/104618)

#### Code Quaity

- Encapsulate raw scripts into functions
- Move log-printing code into functions
