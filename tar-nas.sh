#!/usr/bin/env bash
#"""
# Tar-NAS main user script
#
# The file is intended to be invoked by both ``cron`` and the user.
# The file must be run with root privilege.
#"""

# source global config
THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${THIS_DIR}/conf/global.conf"

touch "$LOG_FILE"
echo "=============================================" >> "$LOG_FILE"
echo "    $(date '+%F %T') tar-nas.sh begins " >> "$LOG_FILE"
echo "=============================================" >> "$LOG_FILE"

# execute files in order
arr=( $( find "$TASKHOME" -name "*.sh" | sort ) )
for f in "${arr[@]}"; do
    echo "$(date '+%F %T') [Debug] sudo bash $f..." >> "$LOG_FILE"
    sudo bash "$f"
done

echo "=============================================" >> "$LOG_FILE"
echo "    $(date '+%F %T') tar-nas.sh done!  " >> "$LOG_FILE"
echo -e "=============================================\n" >> "$LOG_FILE"
