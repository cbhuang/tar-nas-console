#!/usr/bin/env bash
#"""
# Remove crontab entries
#"""

source "conf/global.conf"

echo "$(date '+%F %T') [Info] uninst.sh begins..."

if [ -f "$CRONFILE_SYSTEM" ];

  flag=n
  echo "$(date '+%F %T') [Info] Crontab file found: '$CRONFILE_SYSTEM'"
  read -p "Remove for backup task? (n/y)" flag
  if [ "$flag" == "y" ]; then
    sudo rm "$CRONFILE_SYSTEM"
    echo "$(date '+%F %T') [Info] Tasks are now deregistered from system crontab!"
  fi

else
  echo "$(date '+%F %T') [Info] Nothing has to be done!"
fi

echo "$(date '+%F %T') [Info] uninst.sh done!"
