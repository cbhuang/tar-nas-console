#!/bin/bash
#
# Test script for retention policy using mocked files
#
# Requirements:
#   - A writable NAS device
#   - `global.conf` must be fully properly set
#
# Possible leftovers: a `__test_tar-nas-console` directory under NAS home

# Import global config (do not modify!)
THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "$THIS_DIR/../conf/global.conf"

# Signal program start (do not modify!)
THIS_FILE="$( basename "${BASH_SOURCE[0]}" )"
echo "$(date '+%F %T') [Debug] $THIS_FILE begins..." >> "$LOG_FILE"

# ======================= Parameters =======================

NAS_BACKUP_DIR=Backup/__test_tar-nas-console

N_LATEST=2
N_MONTHLY_LATEST=3
N_QUARTERLY_LATEST=3

backups=(
    # task 1
    "deb12-root_2020-12-28T03-38-58.tar.gz"
    "deb12-root_2021-12-28T03-38-58.tar.gz"
    "deb12-root_2022-10-28T03-38-58.tar.gz"
    "deb12-root_2022-12-28T03-38-58.tar.gz"
    "deb12-root_2023-06-28T03-38-58.tar.gz"
    "deb12-root_2023-08-28T03-38-58.tar.gz"
    "deb12-root_2023-11-26T03-38-58.tar.gz"
    "deb12-root_2023-11-28T03-38-58.tar.gz"
    "deb12-root_2023-12-26T03-38-58.tar.gz"
    "deb12-root_2023-12-28T03-38-58.tar.gz"
    "deb12-root_2024-01-28T03-38-58.tar.gz"
    "deb12-root_2024-02-28T03-38-58.tar.gz"
    "deb12-root_2024-03-28T03-38-58.tar.gz"
    "deb12-root_2024-04-18T03-38-58.tar.gz"
    "deb12-root_2024-04-28T03-38-58.tar.gz"
    "deb12-root_2024-05-18T03-38-58.tar.gz"
    "deb12-root_2024-05-28T03-38-58.tar.gz"
    "deb12-root_2024-06-22T03-38-58.tar.gz"
    "deb12-root_2024-06-25T03-38-58.tar.gz"
    "deb12-root_2024-06-28T03-38-58.tar.gz"
    "deb12-root_2024-07-22T03-38-58.tar.gz"
    "deb12-root_2024-07-25T03-38-58.tar.gz"
    "deb12-root_2024-07-28T03-38-58.tar.gz"
    "deb12-root_2024-08-16T01-09-57.tar.gz"
    "deb12-root_2024-08-19T01-09-57.tar.gz"
    "deb12-root_2024-08-23T01-09-57.tar.gz"
    "deb12-root_2024-08-26T01-09-57.tar.gz"
    # task 2
    "win11-root_2017-12-26T02-52-41.img.gz"
    "win11-root_2018-03-26T02-52-41.img.gz"
    "win11-root_2018-08-26T02-52-41.img.gz"
    "win11-root_2019-08-26T02-52-41.img.gz"
    "win11-root_2020-06-26T02-52-41.img.gz"
    "win11-root_2020-06-28T02-52-41.img.gz"
    "win11-root_2020-07-01T02-52-41.img.gz"
    # irrelevant files
    "nvme0n1_2024-08-20.img.gz"
    "nvme0n1-factory_2024-08-11.img.gz"
    "nvme0n1-layout-gparted_2024-08-20.png"
    "nvme0n1-layout-parted_2024-08-20.txt"
)

deletes_expected=(
    # task 1
    "deb12-root_2022-10-28T03-38-58.tar.gz"
    "deb12-root_2023-06-28T03-38-58.tar.gz"
    "deb12-root_2023-08-28T03-38-58.tar.gz"
    "deb12-root_2023-11-26T03-38-58.tar.gz"
    "deb12-root_2023-11-28T03-38-58.tar.gz"
    "deb12-root_2023-12-26T03-38-58.tar.gz"
    "deb12-root_2024-01-28T03-38-58.tar.gz"
    "deb12-root_2024-02-28T03-38-58.tar.gz"
    "deb12-root_2024-04-18T03-38-58.tar.gz"
    "deb12-root_2024-04-28T03-38-58.tar.gz"
    "deb12-root_2024-05-18T03-38-58.tar.gz"
    "deb12-root_2024-06-22T03-38-58.tar.gz"
    "deb12-root_2024-06-25T03-38-58.tar.gz"
    "deb12-root_2024-07-22T03-38-58.tar.gz"
    "deb12-root_2024-07-25T03-38-58.tar.gz"
    "deb12-root_2024-08-16T01-09-57.tar.gz"
    "deb12-root_2024-08-19T01-09-57.tar.gz"
    # task 2
    "win11-root_2018-03-26T02-52-41.img.gz"
    "win11-root_2020-06-26T02-52-41.img.gz"
)

# ======================= End Parameters =======================

# create mocked files
ssh -p $NAS_SSH_PORT $NAS_USER@$NAS_HOST "mkdir -p '$NAS_BACKUP_DIR'"
ret=$?
if [ $ret -ne 0 ]; then
    echo "$(date '+%F %T') [Error] Failed creating remote test directory! (return code: $ret)" >> "$LOG_FILE"
    exit $ret
fi

for backup in "${backups[@]}"; do
    ssh -p $NAS_SSH_PORT $NAS_USER@$NAS_HOST "touch '$NAS_BACKUP_DIR/$backup'"
    ret=$?
    if [ $ret -ne 0 ]; then
        echo "$(date '+%F %T') [Error] Failed creating mocked backup files! (return code: $ret)" >> "$LOG_FILE"
        exit $ret
    fi
done

source "$SRCHOME/retention.sh"

echo "$(date '+%F %T') [Debug] $THIS_FILE done!" >> "$LOG_FILE"

# check result
n_deletes_expected=${#deletes_expected[@]}

if (( n_deletes == n_deletes_expected )); then
    echo -e "$(date '+%F %T') [Info] Test passed!\n" >> "$LOG_FILE"
else
    echo -e "$(date '+%F %T') [Error] Test failed: #deleted files=$n_deletes but expect $n_deletes_expected!\n" >> "$LOG_FILE"
    exit 1
fi
