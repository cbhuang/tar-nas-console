#!/usr/bin/env bash
#"""
# Program Setup
#"""

echo "$(date '+%F %T') [INFO] setup.sh begins..."

# check existing global config
flag_skip=n
if [ -f "conf/global.conf" ]; then
  read -p "Existing global.conf found. Accept?(y/n)" flag_skip
fi

# set global config
if [ "$flag_skip" == "y" ]; then
  echo "$(date '+%F %T') [Debug] Accept old global.conf!"
else

  # renew local configuration
  if [ -e "conf/global.conf.template" ]; then
    cp -f "conf/global.conf.template" "conf/global.conf"
  else
    echo "$(date '+%F %T') [Error] ./conf/global.conf.template not found!"
    exit 1
  fi

  # set project home
  PRJHOME="$(pwd)"
  sed -i -r "s#^PRJHOME=(.*)#PRJHOME=\"${PRJHOME}\"#" "conf/global.conf"

  # read new global config
  source "conf/global.conf"
  # NOTE: from now, use "${CONFHOME}/global.conf" to reference this file

  # local user
  flag=n
  while [ "$flag" != "y" ]; do
    echo ""
    read -p "Enter your local user name:" LOCAL_USER
    read -p "Enter your local user group:" LOCAL_GROUP
    read -p "OK?(n/y) " flag
  done
  sed -i -r "s#^LOCAL_USER=(.*)#LOCAL_USER=\"${LOCAL_USER}\"#" "conf/global.conf"
  sed -i -r "s#^LOCAL_GROUP=(.*)#LOCAL_GROUP=\"${LOCAL_GROUP}\"#" "conf/global.conf"

  # NAS connection
  flag=n
  while [ "$flag" != "y" ]; do
    echo ""
    read -p "Enter SSH port number:" NAS_SSH_PORT
    read -p "Enter SSH username:" NAS_USER
    read -p "Enter NAS hostname or ip address:" NAS_HOST
    read -p "OK?(n/y) " flag
  done
  sed -i -r "s#^NAS_SSH_PORT=(.*)#NAS_SSH_PORT=\"${NAS_SSH_PORT}\"#" "conf/global.conf"
  sed -i -r "s#^NAS_USER=(.*)#NAS_USER=\"${NAS_USER}\"#" "conf/global.conf"
  sed -i -r "s#^NAS_HOST=(.*)#NAS_HOST=\"${NAS_HOST}\"#" "conf/global.conf"
fi
# end global.conf settings


#"""
# Set System Crontab
#"""
echo "$(date '+%F %T') [Info] Copying the crontab file..."

# load installed
. conf/global.conf

WRITE_CRONFILE=y
if [ -f "$CRONFILE_SYSTEM" ]; then  
  read -p "$CRONFILE_SYSTEM exists! Re-create? (n/y) " WRITE_CRONFILE
else
  read -p "Create cron file ${CRONFILE_SYSTEM}? (n/y) " WRITE_CRONFILE
fi

if [ "$WRITE_CRONFILE" == "y" ]; then
  # Create cronfile within the project
  #   N.B. use "?"" as regex separator that cannot exist in a POSIX path
  sudo sed "s?\$PRJHOME?$PRJHOME?" "$PRJHOME/conf/cron.d.template" > "$CRONFILE_SYSTEM"
  echo "$(date '+%F %T') [Info] Crontab file created: $CRONFILE_SYSTEM"
  echo "$(date '+%F %T') [Info] Please edit the crontab file manually to adjust the schedule!"
else
  echo "$(date '+%F %T') [Info] Crontab file is untouched!"
fi

echo "$(date '+%F %T') [Info] setup.sh finished!"

